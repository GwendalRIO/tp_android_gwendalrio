package fr.enssat.grio.TP_GwendalRIO;

public class MetaData {
    private int curseur;
    private String titre;
    private String url;

    public MetaData(String rTitre, int rCurseur, String rUrl){
        this.curseur = rCurseur;
        this.url = rUrl;
        this.titre = rTitre;
    }

    public int getCurseur(){
        return this.curseur;
    }

    public String getUrl(){
        return this.url;
    }

    public String getTitre(){
        return this.titre;
    }
}