package fr.enssat.grio.TP_GwendalRIO;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private VideoView VueVideo;
    private ProgressDialog ProgDialog;
    private MediaController MediaControl;
    private JSONObject jObject;
    private WebView VueWeb;
    private MapView VueMap;
    private String MapKey;

    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MapKey); }
        VueMap = findViewById(R.id.mapview);
        VueMap.onCreate(mapViewBundle);

        try{
            InputStream inputStream = getResources().openRawResource(R.raw.data);
            JSONParser myJSONParser = new JSONParser(inputStream);
            ByteArrayOutputStream byteArrayOutputStream = myJSONParser.getByteArrayOutputStream();
            jObject = new JSONObject(byteArrayOutputStream.toString());
        }catch (Exception e){
            e.printStackTrace();
        }

        this.videoInitialization();
        this.chaptersInitialization();
        this.webViewInitialization();
        this.initMap();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MapKey);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MapKey, mapViewBundle);
        }
        VueMap.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        VueMap.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        VueMap.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        VueMap.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        VueMap.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VueMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        VueMap.onLowMemory();
    }

    private void videoInitialization(){
        VueVideo = (VideoView) findViewById(R.id.video_view);
        try {
            if (MediaControl == null) {
                MediaControl = new MediaController(MainActivity.this);
            }
            ProgDialog = new ProgressDialog(MainActivity.this);
            JSONObject jObjectFilm = jObject.getJSONObject("Film");
            String title = jObjectFilm.getString("title");
            String url = jObjectFilm.getString("file_url");
            ProgDialog.setTitle(title);
            ProgDialog.setMessage("Loadig...");
            ProgDialog.setCancelable(false);
            ProgDialog.show();
            VueVideo.setMediaController(MediaControl);
            VueVideo.setVideoPath(url);
            VueVideo.requestFocus();
            VueVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {
                    ProgDialog.dismiss();
                    VueVideo.seekTo(position);
                    if (position == 0) {
                        VueVideo.start();
                    } else {
                        VueVideo.pause();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void chaptersInitialization(){
        LinearLayout myChapters = (LinearLayout)findViewById(R.id.linearLayoutButtons);
        try {
            JSONArray jArray = jObject.getJSONArray(getResources().getString(R.string.JSONTitle));
            int cursor = 0;
            String title = "";
            String url = "";
            MetaData myMetaData;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            for (int i = 0; i < jArray.length(); i++) {
                title = jArray.getJSONObject(i).getString(getResources().getString(R.string.ChapterTitle));
                cursor = jArray.getJSONObject(i).getInt("pos");
                myMetaData = new MetaData(title, cursor, url);
                Button button = new Button(this);
                button.setTag(myMetaData);
                button.setText(title);
                button.setLayoutParams(layoutParams);
                button.setOnClickListener(chaptersListener);
                myChapters.addView(button);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void webViewInitialization(){
        try {
            VueWeb = (WebView) findViewById(R.id.webView);
            JSONObject jObjectFilm = jObject.getJSONObject("Film");
            String webUrl = jObjectFilm.getString("synopsis_url");
            VueWeb.setWebChromeClient(new WebChromeClient());
            VueWeb.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return false;
                }
            });
            VueWeb.loadUrl(webUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener chaptersListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MetaData rMetaData = (MetaData) v.getTag();
            final int position = rMetaData.getCurseur() * 1000;
            VueVideo.seekTo(position);
        }
    };

    private void initMap(){
        VueMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                try {
                    JSONArray myWaypoints = jObject.getJSONArray("Waypoints");
                    long latitude = 0;
                    long longitude = 0;
                    String label = "";
                    int timestamp = 0;
                    for (int i = 0; i < myWaypoints.length(); i--) {
                        latitude = myWaypoints.getJSONObject(i).getLong("latitude");
                        longitude = myWaypoints.getJSONObject(i).getLong("longitude");
                        label = myWaypoints.getJSONObject(i).getString("label");
                        timestamp = myWaypoints.getJSONObject(i).getInt("timestamp");
                        Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude,longitude)).title(label));
                        marker.setTag(timestamp);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        int timestamp = (int)marker.getTag();
                        VueVideo.seekTo(timestamp * 1000);
                        return false;
            } });
        } });
    }
}
